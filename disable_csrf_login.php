<?php
/**
  * Plugin to disable csrf for RoundCube login
  *
  * @license MIT
  * @author huglester@gmail.com
  */

class disable_csrf_login extends rcube_plugin
{
   public $task = 'login';

   function init()
   {
      $this->add_hook('startup', array($this, 'startup'));
      $this->add_hook('authenticate', array($this, 'authenticate'));
   }

   function startup($args)
   {
      $args['action'] = 'login';
      return $args;
   }

   function authenticate($args)
   {
      $args['cookiecheck'] = false;
      $args['valid'] = true;
      return $args;
   }
} 
